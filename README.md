# Sorting Screws With Computer Vision & Machine Learning Object Detection

The object detection portion of this project is based off the tutorial [https://github.com/EdjeElectronics/TensorFlow-Object-Detection-API-Tutorial-Train-Multiple-Objects-Windows-10]
With this we were able to train our own model for detecting screw head-types.

## Brief Summary
*Last updated: 5/8/2019 with Python 3.5 & TensorFlow v1.10*

The comprehensive system is run through the Screw_detector.py script. This sets up the camera to take a picture of the screw, then uses the Length_width2.py script to 
identify its characteristics and output a control signal for the sorting mechanism.


The system runs as follows:

1. [Detect if screw is in frame](https://gitlab.com/ajmcgrath/Image-Rec#1-detect-if-screw-is-in-frame)
2. [Take picture and run image proccessing to find length & width](https://gitlab.com/ajmcgrath/Image-Rec#2-take-picture-and-run-image-proccessing-to-find-length-and-width)
3. [Use trained tensorflow model to classify head-type](https://gitlab.com/ajmcgrath/Image-Rec#3-use-trained-tensorflow-model-to-classify-head-type)
4. [Sort screws based on the three identified characteristics](https://gitlab.com/ajmcgrath/Image-Rec#4-sort-screws-based-on-the-three-identified-characteristics)


### 1. Detect if screw is in frame
The idea behind this algorithm is to compare the first 25 rows of the input frame with that of the previous frame. When there is nothing on the conveyor belt this portion of the frame will be
similar to the previous frame. When we detect a noticeable difference in these first few rows, we know the beginning of a screw has entered the frame. We then run a similar algorithm where we 
are waiting for the first 25 rows to become similar to the first 25 rows again. This would show us that the screw is fully in frame because the pixels at the top now match the conveyor belt 
with no screw on it. The pictures below show this process :
    
<p align="center">
    <img src="doc/detection.JPG">
</p>    

The thresholds described above are in the LiveSeeScrew.py script in the Raspicode folder. This folder contains all of of the code that sits on the pi and is run through an ssh channel on the main 
computer. They are found at lines 312 , 321 , 364 , 378 . The first and third of these values are used to decipher how off the color must be in order to declare it a bad pixel. This number can be 
dependent on the light so it may need to be adjusted. Ideally a consistent lighting environment could be created where these values need not be changed. The second and fourth values decide how many 
bad pixels are required to considered the image different (whether or not a screw is in frame). These can also be adjusted if the conveyor belt being used has a somewhat inconsistent background.
    

### 2. Take picture and run image proccessing to find length and width

#### 2a. Background Subtraction
Once the LiveSeeScrew.py code completes a picture of the screw will be stored on the pi. This must be transferred to the main computer which we are doing through an sftp channel. The 
ip addresses required for this channel, as well as ssh, must be updated in the connectSSH.py & connectSFTP.py scripts. This image is then compared with an image of the conveyor (without 
a screw) in order to find the screw object. 
<p align="center">
    <img src="doc/b&w.JPG">
</p>

#### 2b. Align Screw Vertically
This is the basic outline of our screw object that is used to detect length and width. We then run the original input image through a line detector in order to identify its orientation. 
The line detector allows us to determine the slope of the screw which in turn gives us the angle from vertical. We can then use this value to rotate the image (filling with black) so that 
the screw is 90 degrees vertical.
<p align="center">
    <img src="doc/line_detect.JPG">
</p>

#### 2c. Use contours to find length & width
With this vertically oriented screw object we can now use the cv2.threshold function to outline the screw with identifiable pixels. These can then be used to identify an average
for the length and width of the screw.The first pixel in the screw list is at the top middle of the screw and the last meets back up at the top middle of the screw (head). With 
this in mind, the width can be found by ignoring the first and last fifth of the screw list. This will remove all pixels that are part of the head. We can then use the identified 
bottom most pixel in order to detect where the left side finishes and the right side starts. With this, we can identify an average left side value and right side value where the difference 
(right - left) is the width of the screw. A similar process is used to identify a top and bottom average for the screw. The difference here is we cannot remove the left or right side because 
we can't be sure where in the list they are. Instead, we use the distance from the left/right averages, we have identified, to know where on the screw we are located. The top and bottom 
average is then calculated where bottom - top = length. The length portion of this algorithm has an occasional bug where the bottom avg is never found. Having yet to identify a solution to
this problem, when it occurs the bottom average is simply set to the bottom most pixel that was identified at the beginning.

<p align="center">
    <img src="doc/l_w.JPG">
</p>

#### 2d. Upsidedown detection
In order for the for the characteristics to be identified properly the screw should be oriented head-side up (this could be adjusted). With this in mind, we must detect whether or not a screw 
is oriented correctly before outputting results. This is done by splitting the screw object in half and checking the area of the top and bottom. The top area should be greater if oriented 
correctly as the head will always be wider than the tail. If this is not the case, we simply flip the image and rerun the code from the begging.
<p align="center">
    <img src="doc/upsidedown.JPG">
</p>


### 3. Use trained tensorflow model to classify head-type
With the length and width characteristics now identified, we run the original image through our trained TensorFlow model. With this we can identify the head-type of the screw. In order to increase 
the speed, we initialize the model at the beginning with a test image which speeds up future runs.
<p align="center">
    <img src="doc/tflow.JPG">
</p>

### 4. Sort screws based on the three identified characteristics
Now having three distinct characteristics for the screw we can run it through our database to know whether or not it has been seen in this session. If not, it will be placed in its own container. 
If so, it will be placed with its like screw. Our system has a total of six containers so it can sort up to five different screws in a session with the six bin holding all extra screws, as well 
as any screws that ran into errors during identification. These screws in the sixth bin must be rerun in a new session.
<p align="center">
    <img src="doc/fullsys.JPG">
</p>

## Error Handling
Our system currently handles three different types of errors. The first is a bad detection, this occurs when the beginning of the screw is detected to be in frame on the conveyor belt, but the systems 
takes too long for the screw to be fully in frame. This prevents the system from getting stuck in the detection phase. The next is a bad identification which occurs if the screw object detected does 
not fit inside the specifications of the length and width (see below). The third is any runtime errors that may occur, these are all passed with the screw being sent to the sixth bin.

## Specifications 
Our final specifications were screws between 1/2 " - 2&1/4 " . This could be increased or decreased by changing the distance of the camera to the conveyor. The total time to sort a single screw from 
start to finish is 8 seconds. For a session of 150 screws this would take 20 minutes.

