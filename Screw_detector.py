from Length_width2 import detect
from Length_width2 import find_head_type
from Length_width2 import check_match
from connectSSH import *
from connectSFTP import *
import time
import os
import tensorflow as tf
import numpy as np
from utils import label_map_util
from utils import visualization_utils as Vis_utils
import subprocess
import cv2

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

MODEL_NAME = 'screw_model'
IMAGE_NAME = './images/ace1.jpg'

# Grab path to current working directory
CWD_PATH = os.getcwd()

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
PATH_TO_CKPT = os.path.join(CWD_PATH, MODEL_NAME, 'frozen_inference_graph.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH, 'training', 'screw_labelmap.pbtxt')

# Path to image
PATH_TO_IMAGE = os.path.join(CWD_PATH, IMAGE_NAME)

# Number of classes the object detector can identify
NUM_CLASSES = 4

# Load the label map.
# Label maps map indices to category names, so that when our convolution
# network predicts `5`, we know that this corresponds to `king`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
															use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
	od_graph_def = tf.GraphDef()
	with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
		serialized_graph = fid.read()
		od_graph_def.ParseFromString(serialized_graph)
		tf.import_graph_def(od_graph_def, name='')

	sess = tf.Session(graph=detection_graph)

# Define input and output tensors (i.e. data) for the object detection classifier

# Input tensor is the image
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

# Output tensors are the detection boxes, scores, and classes
# Each box represents a part of the image where a particular object was detected
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

# Each score represents level of confidence for each of the objects.
# The score is shown on the result image, together with the class label.
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

# Number of objects detected
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

image = './images/back'+ str(1) + '.jpg'
image = cv2.imread(image)
image_expanded = np.expand_dims(image, axis=0)

# Perform the actual detection by running the model with the image as input
(boxes, scores, classes, num) = sess.run(
	[detection_boxes, detection_scores, detection_classes, num_detections],
	feed_dict={image_tensor: image_expanded})


ssh_string = 'sudo python3 takeOrig.py'
#runCommand(ssh_string)
pi_img = 'd' + '.jpg'
sftp_string_pi = '/home/pi/' + pi_img
sftp_string_cp = './images/back'+ str(1) + '.jpg'
runSFTPCommand(2, sftp_string_pi, sftp_string_cp)

Bad_detect = 0
Bad_id = 0
id_err = 0
output = 0

x = 1
out_val = True

while x < 9 :
	l = -1
	w = -1
	if(x > 1) : out_val = False

	ssh_string = 'sudo python3 LiveSeeScrew.py -z d'+str(x)+'.jpg'				#run screw detector code on pi (waits for full screw to be seen)

																			#todo add raspistill command here
	pi_img = 'd' + str(x) + '.jpg'
	#pi_img = 'back' + str(2) + '.jpg'
	sftp_string_pi = '/home/pi/' + pi_img
	sftp_string_cp = './images/d' + str(x) + '.jpg'
	#sftp_string_cp = './images/back'+ str(1) + '.jpg'
	#os.system('pause')																		#wait for key press (removed in full system


	print("Detecting Screw: ", x)
	out_image = './images/WaitImg.JPG'
	out_image = cv2.imread(out_image)
	winname = "Wait Image"
	cv2.namedWindow(winname)  # Create a named window
	cv2.moveWindow(winname, 3620, 310)
	cv2.imshow(winname, out_image)
	cv2.waitKey(100)
	output = 0
	output = runCommand(ssh_string)
	cv2.destroyAllWindows()



	if out_val :
		p = subprocess.Popen([sys.executable, 'Stop_conveyor.py'],
							 stdout=subprocess.PIPE,
							 stderr=subprocess.STDOUT)

	if output == "Bad Detect\n" :
		print("Bad Detect: ", x)
		screw_loc = 6
		Bad_detect = Bad_detect + 1
	else :
		print("Seen Screw: ", x)
		#sftp image to the pi
		runSFTPCommand(2, sftp_string_pi, sftp_string_cp)


		picture = 'images/d'+str(x)				#x
		picture = picture + '.jpg'

		try :
			crop, l, w = detect(picture, out_val)											#get the h/w of screw with detect
			#card = cv2.imread('images/h' + str(x) + '.jpg')

			if (w == -2):
				screw_loc = 6
				print("Bad Id")
				Bad_id = Bad_id + 1
			else:

				#percentage, id = find_head_type(crop, out_val)  # get the headtype of the screw with find_head_type

				image = crop
				image_expanded = np.expand_dims(image, axis=0)

				#start = time.perf_counter()
				# Perform the actual detection by running the model with the image as input
				(boxes, scores, classes, num) = sess.run(
					[detection_boxes, detection_scores, detection_classes, num_detections],
					feed_dict={image_tensor: image_expanded})

				# Draw the results of the detection (aka 'visulaize the results')

				#end = time.perf_counter()
				#print(end - start)


				# All the results have been drawn on image. Now display the image.

				percentage = np.squeeze(scores)[0]
				id = np.squeeze(classes)[0]

				if out_val:
					p = subprocess.Popen([sys.executable, 'Start_conveyor.py'],
										 stdout=subprocess.PIPE,
										 stderr=subprocess.STDOUT)


					Vis_utils.visualize_boxes_and_labels_on_image_array(
						image,
						np.squeeze(boxes),
						np.squeeze(classes).astype(np.int32),
						np.squeeze(scores),
						category_index,
						use_normalized_coordinates=True,
						line_thickness=8,
						min_score_thresh=0.80)


					orig = image.copy()
					height, width, channels = orig.shape
					h = 650  # int(height / (sizefactor / 2.75))
					w_pic = 300  # int(width / (sizefactor / 2.75))
					img = cv2.resize(orig, (w_pic, h))  # Resize image
					winname = "TensorFlow"
					cv2.namedWindow(winname)  # Create a named window
					cv2.moveWindow(winname, 4200, 0)
					cv2.imshow(winname, orig)

					# Press any key to close the image
					cv2.waitKey(3000)

					# Clean up
					cv2.destroyAllWindows()



				screw_loc = check_match(id, percentage, l, w)  # id, percentage

				if (percentage < 0.6):
					id = -1

				if (id == 1.0 ) : id = "Bolt"
				if (id == 2.0) : id = "Cap"
				if (id == 3.0) : id = "Machine"
				if (id == 4.0) : id = "Wood"


		except :
			screw_loc = 6
			print("Error in ID")
			time.sleep(2)
			cv2.destroyAllWindows()
			id_err = id_err + 1
			pass





	ssh_string = "sudo python conveyor.py"
	#runCommand(ssh_string)  # screw_loc to pi via ssh command				#turn servo to the correct location

	print("loc", screw_loc, l, w, id)  # percentage)
	# print(screw_list)
	ssh_string = "sudo python servo.py -s " + str(screw_loc)
	runCommand(ssh_string) # screw_loc to pi via ssh command				#turn servo to the correct location

	x = x + 1


time.sleep(3.5)
ssh_string = "sudo python conveyor_stop.py"
runCommand(ssh_string)  # screw_loc to pi via ssh command
print()
print()
print()
print("*******************")
print("Session Info")
print("Bad Detects: ", Bad_detect)
print("Bad Id's: ", Bad_id)
print("Id Errors: ", id_err)
print("Total Bad Screws: ", (Bad_id+Bad_detect+id_err))
print("% Bad: ", ((Bad_id+Bad_detect+id_err)/(x-1)))
print("Session Done")
print("*******************")

