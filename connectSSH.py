import paramiko
import sys

nbytes = 4096
hostname = '169.254.19.4'
port = 22
username = 'pi'
password = 'lighthouse20'
command = ""

def runCommand (command1):
    client = paramiko.Transport((hostname, port))
    client.connect(username=username, password=password)

    stdout_data = []
    stderr_data = []
    session = client.open_channel(kind='session')
    session.exec_command(command1)
    while True:
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
            out_line = b''.join(stdout_data).decode()
            #print("|",out_line,"|")
            if(out_line == "Done\n") :
                print("here\n")
                break
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            break

    #print('exit status: ', session.recv_exit_status())
    ret_string = b''.join(stdout_data).decode()
    #print(b''.join(stdout_data).decode())
    #print(b''.join(stderr_data).decode())

    session.close()
    client.close()
    return ret_string

if __name__ == "__main__":
   # stuff only to run when not called via 'import' here
   runCommand("ls")


