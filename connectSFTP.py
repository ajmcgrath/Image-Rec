import paramiko
import sys

nbytes = 4096
hostname = '169.254.19.4'
port = 22
username = 'pi'
password = 'lighthouse20'


def runSFTPCommand (putpush, command1,command2):
    client = paramiko.Transport((hostname, port))
    client.connect(username=username, password=password)

    #print("FIRST PARAM=1 for PUSH and 2 for PULL\n")
    #print("NOTE:PATHS MUST BE IN BETWEEN '' NOT QUOTATION\n")

    sftp = paramiko.SFTPClient.from_transport(client)

    if (putpush==1):
        sftp.put(command1, command2)

    elif(putpush==2):
        sftp.get(command1, command2)

    else:
        print("Wrong Argument")

    sftp.close()
    client.close()


if __name__ == "__main__":
   # stuff only to run when not called via 'import' here
   runSFTPCommand(2,'jordanisahoe.txt','YUP.txt')