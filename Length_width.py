# USAGE
# python object_size.py --image images/example_01.png --width 0.955
# python object_size.py --image images/example_02.png --width 0.955
# python object_size.py --image images/example_03.png --width 3.5

# import the necessary packages
from scipy.spatial import distance as dist
from imutils import perspective
from imutils import contours
from PIL import Image

#from PIL import Image

import numpy as np
import argparse
import imutils
import cv2


#globals
sizefactor = 4

#from PIL import Image

def midpoint(ptA, ptB):
	return ((ptA[0] + ptB[0]) * 0.5, (ptA[1] + ptB[1]) * 0.5)


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to the input image")
ap.add_argument("-w", "--width", type=float, required=True,
	help="width of the left-most object in the image (in inches)")
args = vars(ap.parse_args())


#test to show image
#img = Image.open("image")
#img.show()

# load the image, convert it to grayscale, and blur it slightly
image = cv2.imread('images/a1.jpeg') #args["image"])			#add args again

OG = image

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = cv2.GaussianBlur(gray, (5, 5), 0)
sobelx = cv2.Sobel(gray,cv2.CV_64F,1,0,ksize=5)
sobely = cv2.Sobel(gray,cv2.CV_64F,0,1,ksize=5)

# perform edge detection, then perform a dilation + erosion to
# close gaps in between object edges

edged = cv2.Canny(gray, 5, 80)
edged = cv2.dilate(edged, None, iterations=5)
edged = cv2.erode(edged, None, iterations=5)

# find contours in the edge map
cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0] if imutils.is_cv2() else cnts[1]



# sort the contours from left-to-right and initialize the
# 'pixels per metric' calibration variable
(cnts, _) = contours.sort_contours(cnts)
pixelsPerMetric = None

cntlist = []

for c in cnts:							#remove contours smaller than certain size
	if cv2.contourArea(c) > 1000:		#can be calibrated to smallest possible screw
		cntlist.append(c)


size = len(cntlist)
if size < 3 :					#todo remove this should always be 3 objects
	screw = []					#2 objects once quarter is removed
	quarter = []
	quarter.append(cntlist[0])		#seperate contours
	screw.append(cntlist[1])

	xlast = 0
	ylast = 0
	yfin = 0
	xfin = 0
	bigdif = 0
	xmax = 0
	ymax = 0
	posshead  = []			#possible head locations

	for a in range(0,len(screw[0])):		#rows		#todo adjust code for horizontal placement
		#for b in range(0, screw.shape[1]):	#cols
		coord = screw[0][a]
		coord = coord[0]
		x, y = coord				#get coordinates of screw contour point
		"""
		#five is arbitrary have to figure out, slope between the 2?
		#switch to y for horizontal
		"""
		if abs(x-xlast) > 5:			#check if contour is shifting significantly
			if x!=0 and xlast!=0 :		#ensure it is an actual point (could remov?)
				dif = abs(x-xlast)		#store diff in pixel x val (could use slope?)
				if y>yfin and y>=ylast : #check if larger than prevoius max distance at location yfin, second condition prevents going back up other side of screw
					posshead.append([y]) #add as possible head starting point
					yfin = y			#store new location values
					xmax = x
					bigdif = dif
		xlast = x			#set values for iteration
		ylast = y
		if y>ymax :			#get the maximum value for y to prevent head at bottom bug
			ymax = y
	if yfin == ymax :
		yfin = posshead[len(posshead)-2]	#choose second largest xdifference if highest is at bottom of the screw
	tail = []
	head = []
	quart = []
	if yfin!=0 :
		for a in range(0,len(screw[0])):		#iterate through screw array
			coord = screw[0][a]					#adding points above the yfin val
			coord = coord[0]					#to head and below to tail
			x, y = coord
			if y > yfin : #and x <= (xmax):
				tail.append([x,y]) 	# = np.append(tail, np.array([[x, y]]))
			else :
				head.append([x,y])
		for a in range(0,len(quarter[0])):		#ensures consistent datatypes for quarter array
			coord = quarter[0][a]
			coord = coord[0]
			x, y = coord
			quart.append([x,y])
	t = []										#turn each object to contour numpy array
	t.append(tail)
	tail = np.array(t).astype('int32')
	h = []
	h.append(head)
	head = np.array(h).astype('int32')
	q = []
	q.append(quart)
	quarter = np.array(q).astype('int32')

	#draw the contours of the 3 objects in B,G,R
	#cv2.drawContours(image, quarter, -1, (255,0,0),2)    #uncomment to see the contours drawn
	#cv2.drawContours(image, head, -1, (0,255,0),2)
	#cv2.drawContours(image, tail, -1, (0,0,255),2)
	#cv2.circle(image, (1092, 725), 32, (0, 0, 255), 1, 8);

	cnts = quarter, head, tail			#combine objects for iteration
	cnts = list(cnts)
										#todo could maybe increase accuracy by taking distance between points with same axis

# loop over the contours individually
for c in cnts:
	# if the contour is not sufficiently large, ignore it
	#print(cv2.contourArea(c))
	if cv2.contourArea(c) < 500:			#could change this
		continue

	# compute the rotated bounding box of the contour
	orig = image.copy()
	box = cv2.minAreaRect(c)
	box = cv2.cv.BoxPoints(box) if imutils.is_cv2() else cv2.boxPoints(box)
	box = np.array(box, dtype="int")

	# order the points in the contour such that they appear
	# in top-left, top-right, bottom-right, and bottom-left
	# order, then draw the outline of the rotated bounding
	# box
	box = perspective.order_points(box)

	cv2.drawContours(orig, [box.astype("int")], -1, (0, 255, 0), 1)	#cv2.drawContours(orig, [box.astype("int")], -1, (0, 255, 0), 2)





	xmax = 0
	xmin = 3000
	ymin = 3000
	ymax = 0
	# loop over the original points and draw them
	for (x, y) in box:
		cv2.circle(orig, (int(x), int(y)), 5, (0, 0, 255), -1)
		if x < xmin :								#find corners of image for cropping
			xmin = x
		if x > xmax :
			xmax = x
		if y < ymin :
			ymin = y
		if y > ymax :
			ymax = y
		crop_img = OG[int(ymin)-1:int(ymax)+1, int(xmin)-1:int(xmax)+1]			# store cropped version of image with one pixel of freedom on each edge


	# unpack the ordered bounding box, then compute the midpoint
	# between the top-left and top-right coordinates, followed by
	# the midpoint between bottom-left and bottom-right coordinates
	(tl, tr, br, bl) = box
	(tltrX, tltrY) = midpoint(tl, tr)
	(blbrX, blbrY) = midpoint(bl, br)



	# compute the midpoint between the top-left and top-right points,
	# followed by the midpoint between the top-righ and bottom-right
	(tlblX, tlblY) = midpoint(tl, bl)
	(trbrX, trbrY) = midpoint(tr, br)

	# draw the midpoints on the image
	cv2.circle(orig, (int(tltrX), int(tltrY)), 5, (255, 0, 0), -1)
	cv2.circle(orig, (int(blbrX), int(blbrY)), 5, (255, 0, 0), -1)
	cv2.circle(orig, (int(tlblX), int(tlblY)), 5, (255, 0, 0), -1)
	cv2.circle(orig, (int(trbrX), int(trbrY)), 5, (255, 0, 0), -1)

	# draw lines between the midpoints
	cv2.line(orig, (int(tltrX), int(tltrY)), (int(blbrX), int(blbrY)),
		(255, 0, 255), 2)
	cv2.line(orig, (int(tlblX), int(tlblY)), (int(trbrX), int(trbrY)),
		(255, 0, 255), 2)

	# compute the Euclidean distance between the midpoints
	dA = dist.euclidean((tltrX, tltrY), (blbrX, blbrY))
	dB = dist.euclidean((tlblX, tlblY), (trbrX, trbrY))


	# if the pixels per metric has not been initialized, then
	# compute it as the ratio of pixels to supplied metric
	# (in this case, inches)
	if pixelsPerMetric is None:
		pixelsPerMetric = dB / args["width"]

	# compute the size of the object
	dimA = dA / pixelsPerMetric
	dimB = dB / pixelsPerMetric

	print(dimA)						#print full dimensions of screw
	print(dimB)

	# draw the object sizes on the image
	cv2.putText(orig, "{:.1f}in".format(dimA),
		(int(tltrX - 15), int(tltrY - 10)), cv2.FONT_HERSHEY_SIMPLEX,
		(0.65 * sizefactor), (0, 0, 0), 2)
	cv2.putText(orig, "{:.1f}in".format(dimB),
		(int(trbrX + 10), int(trbrY)), cv2.FONT_HERSHEY_SIMPLEX,
		(0.65 * sizefactor), (0, 0, 0), 2)



	# show the output image
	height, width, channels = orig.shape
	h = int(height/(sizefactor/2))
	w = int(width/(sizefactor/2))
	img = cv2.resize(orig, (w, h)) # Resize image
	cv2.imshow("Image", img)
	cv2.waitKey(0)

	# show cropped version of each object
	height, width, channels = crop_img.shape
	h = int(height / (sizefactor / 2))
	w = int(width / (sizefactor / 2))
	img = cv2.resize(crop_img, (w, h))  # Resize image
	cv2.imshow("Image", img)
	cv2.waitKey(0)
	cropped = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)		#save the cropped image in gray
	cropped = Image.fromarray(cropped)							#could be used to store head and send to machine learning model
	cropped.save("cropped.jpg")


