# USAGE
# python object_size.py --image images/example_01.png --width 0.955
# python object_size.py --image images/example_02.png --width 0.955
# python object_size.py --image images/example_03.png --width 3.5

# import the necessary packages
from scipy.spatial import distance as dist
from imutils import perspective
from imutils import contours
from PIL import Image

# from PIL import Image

import numpy as np
import argparse
import imutils
import cv2

# globals
sizefactor = 4
screw_list = []

import os
import cv2
import numpy as np
import tensorflow as tf
import scipy.misc
import time
from utils import label_map_util
from utils import visualization_utils as Vis_utils
import sys
import imutils
from skimage.measure import compare_ssim

sys.path.append("..")

from PIL import Image
from scipy import ndimage

# Import packages


# This is needed since the notebook is stored in the object_detection folder.
# import utilites



# from utils import visualization_utils as vis_util

def oldHead_remove(image_file, upsidedown) :    #new  version is screw id
    # load the image, convert it to grayscale, and blur it slightly
    image = cv2.imread(image_file)  # args["image"])			#add args again
    if upsidedown:
        image = np.rot90(image)
        image = np.rot90(image)

    cntlist, image = id_screw(image)

    screw = []  # 2 objects once quarter is removed
    screw.append(cntlist[0])

    coord = cntlist[0][0]  # find distance between contours with similar (3 pixels) y values
    coord = coord[0]
    x, y = coord

    # init variables
    ytot = 0
    coord = screw[0][0]
    coord = coord[0]
    _, y1 = coord
    dif = 0
    ytop = 3000
    ybottom = 0

    screw = screw[0]

    screw = list(screw)

    i = 0
    count = 0

    while i < len(screw) - 1: #limit the number of contour points to increase speed and accuracy
        coord = screw[i]
        _, y = np.split(coord, [-1], axis=1)
        y = int(y)

        if y > ybottom:
            ybottom = y

        if y < ytop:
            ytop = y

        else:
            dif = dif + y - y1
            ytot = ytot + 1
            y1 = y

        if count % 5 != 0:
            del screw[i]  # screw = np.delete(screw, [[x, y]])
        else:
            i = i + 1
        count = count + 1

    a = 1
    con_size = len(screw)
    b = con_size - a - 1
    ydif = dif / 200  # ytot  # take avg dist between y vals
    ydif = abs(int(ydif)) + 1
    ydif = ydif * 5
    yrange = ybottom - ytop
    y_mid = yrange / 2
    xwidth = 0

    screw = np.array(screw, dtype='int32')
    s = []
    s.append(screw)
    screw = np.array(s).astype('int32')

    wid_count = 0
    y_up = ytop
    y_down = ybottom
    topside = True
    first = True
    mid_flag = True

    while ((b - a) > 2):  # find width and check if contour is correct

        coord = screw[0][b]
        coord = coord[0]
        _, testy = coord
        if y1 > testy:
            b = b - 1
            continue
        else:
            x1, y1 = coord
            coord = screw[0][b - 1]
            coord = coord[0]
            xprev1, _ = coord
            coord = screw[0][b + 1]  # find distance between contours with similar (3 pixels) y values
            coord = coord[0]
            xnext1, _ = coord

        while (abs(y1 - y) > ydif):  # traverse  array until find similar y position
            a = a + 1
            if a >= (len(screw[0]) - 1):
                break
            coord = screw[0][a]
            coord = coord[0]
            x, y = coord
            coord = screw[0][a - 1]  # find distance between contours with similar (3 pixels) y values
            coord = coord[0]
            xprev, _ = coord
            coord = screw[0][a + 1]  # find distance between contours with similar (3 pixels) y values
            coord = coord[0]
            xnext, _ = coord
            xavg = int((x + xprev + xnext) / 3)
            xavg1 = int((x1 + xprev1 + xnext1) / 3)

            if (xavg1 - xavg) < 1:  # threshold for bad pixels
                if topside:
                    y_up = y


                else:
                    y_down = y

                    b = 0
                    a = 0
                    y1 = 0
                    y = 0
                    break

            else:
                if first:
                    topside = False  # add bool to do once
                    first = False
                if mid_flag and y >= ytop + y_mid:  # + 30:
                    xwidth = xwidth * wid_count + (xavg1 - xavg)
                    wid_count = wid_count + 1
                    xwidth = int(xwidth / wid_count)
                    # cv2.line(image, (x, y), (x1, y1), (0, 255, 255), 5)             #yellow line
                    mid_flag = False

        b = b - 1  # decrement comparing position

    new = []
    for a in range(0, len(screw[0])):  # iterate through screw array removing contours with width < 50
        coord = screw[0][a]
        coord = coord[0]
        x, y = coord
        if y < y_down and y > y_up:
            new.append([x, y])

    if new:  # find width and length of corrected contour if needed
        n = []
        n.append(new)
        new = np.array(n).astype('int32')

        ytop = y_up
        ybottom = y_down
        yrange = ybottom - ytop

        con_size = len(new[0])
        a = 1
        b = con_size - a - 1
        ydif = dif / ytot  # take avg dist between y vals
        ydif = abs(int(ydif)) + 1
        ydif = ydif * 5
        y_mid = yrange / 2
        xwidth = 0

        screw = new

        coord = screw[0][a]  # find distance between contours with similar (3 pixels) y values
        x, y = coord
        coord = screw[0][a - 1]  # find distance between contours with similar (3 pixels) y values
        xprev, _ = coord
        coord = screw[0][a + 1]  # find distance between contours with similar (3 pixels) y values
        xnext, _ = coord

        mid_flag = 1
        wid_count = 0

        # print(ydif)

        while ((b - a) > 2):  # find width at midpoint

            coord = screw[0][b]
            _, testy = coord
            if y1 > testy:
                b = b - 1
                continue
            else:
                x1, y1 = coord
                coord = screw[0][b - 1]
                xprev1, _ = coord
                coord = screw[0][b + 1]  # find distance between contours with similar (3 pixels) y values
                xnext1, _ = coord

            while (abs(y1 - y) > ydif):  # traverse  array until find similar y position
                a = a + 1
                if a >= (len(screw[0]) - 1):
                    break
                coord = screw[0][a]
                x, y = coord
                coord = screw[0][a - 1]  # find distance between contours with similar (3 pixels) y values
                xprev, _ = coord
                coord = screw[0][a + 1]  # find distance between contours with similar (3 pixels) y values
                xnext, _ = coord
                xavg = int((x + xprev + xnext) / 3)
                xavg1 = int((x1 + xprev1 + xnext1) / 3)

                if mid_flag == 1 and y >= ytop + y_mid:  # + 30:
                    xwidth = xwidth * wid_count + (xavg1 - xavg)
                    wid_count = wid_count + 1
                    xwidth = int(xwidth / wid_count)
                    # cv2.line(image, (x, y), (x1, y1), (0, 0, 255), 5)
                    mid_flag = 0
                    b = 0  # end outside while
                    a = 0
                    break

            b = b - 1  # decrement comparing position

        # print("vals: ", ytop,ybottom)
        # Used for testing contours of head/tail
        # cv2.drawContours(image, new, -1, (0, 255, 0), 2)
        # cv2.drawContours(image, tail, -1, (0,0,255),2)
        # cv2.circle(image, (700, 1041), 32, (0, 0, 255), 1, 8);

        # show the output image

        orig = image.copy()
        height, width, channels = orig.shape
        h = int(height / (sizefactor / 1))
        w = int(width / (sizefactor / 1))
        img = cv2.resize(orig, (w, h))  # Resize image
        cv2.imshow("Image", img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    top = []
    bot = []
    """
    for i in range(0, len(screw[0])):
        coord = screw[0][i]  # find distance between contours with similar (3 pixels) y values
        #coord = coord[0]
        x, y = coord
        if y <= ytop + y_mid:
            top.append([x, y])
        else:
            bot.append([x, y])

    t = []  # turn each object to contour numpy array
    t.append(top)
    top = np.array(t).astype('int32')
    b1 = []
    b1.append(bot)
    bot = np.array(b1).astype('int32')

    #top_area = cv2.contourArea(top)
   # bot_area = cv2.contourArea(bot)

    # cv2.drawContours(image, top, -1, (0, 255, 0), 2)  # uncomment to see the contours drawn
    # cv2.drawContours(image, bot, -1, (0, 0, 255), 2)  #

    # show the output image
    # connect dots
   """
    orig = image.copy()  # blue dots
    height, width, channels = orig.shape
    h = int(height / (sizefactor / 2))
    w = int(width / (sizefactor / 2))
    img = cv2.resize(orig, (w, h))  # Resize image				# could put a point at each postion of the drawn contour
    cv2.imshow("Image", img)
    cv2.waitKey(0)

    # if top_area < bot_area:				#uncomment upside down
    #	print("UpsideDown")
    #	return -1

    ylength = yrange  # ybottom - yfin  #num of pixels change to yfin when head loc is more accurate

    return -1, ylength, xwidth  # replace -1 with image for head type detection




def midpoint(ptA, ptB):
    return ((ptA[0] + ptB[0]) * 0.5, (ptA[1] + ptB[1]) * 0.5)


def oldbackgroundDetect(image):             #no longer works with this version used troublesome frame diff code
    # gray2 = cv2.GaussianBlur(gray2, (5, 5), 0)  # (5,5)
    # gray2 = np.invert(gray2)
    cnts, image = background(image)

    return cnts, image

    edged = cv2.Canny(image, 195, 200)  # 5,95
    edged = cv2.dilate(edged, None, iterations=20)  # 6
    edged = cv2.erode(edged, None, iterations=5)
    edged = np.invert(edged)

    height, width = edged.shape
    h = int(height / (sizefactor / 2))
    w = int(width / (sizefactor / 2))
    img = edged.copy()
    # img = line_image.copy()
    img = cv2.resize(img, (w, h))  # Resize image
    cv2.imshow("DOG", img)
    cv2.waitKey(0)

    return edged

def origCntDetect(image) :                          #problems with shadows and detecting guards
    #gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #gray = cv2.bitwise_not(gray)
    gray = cv2.GaussianBlur(image, (1, 1), 0)  # (5,5)

    height, width,_ = gray.shape
    h = int(height / (sizefactor / 2))
    w = int(width / (sizefactor / 2))
    img = gray.copy()
    # img = line_image.copy()
    img = cv2.resize(img, (w, h))  # Resize image
    cv2.imshow("DOG", img)
    cv2.waitKey(0)


    sobelx = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=5)
    sobely = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=5)

    edged = cv2.Canny(gray, 50, 75)  # 5, 35 || 35, 95
    kernal = np.ones((3, 3), np.uint8)

    #edged = cv2.Canny(gray, 5, 35)  # 5,95

    #edged = cv2.dilate(edged, None, iterations=1)
    #edged = cv2.erode(edged, None, iterations=1)
    edged = cv2.dilate(edged, kernal, iterations=10)  # 6
    edged = cv2.erode(edged, kernal, iterations=1)

    # show the output image B&W

    height, width = edged.shape
    h = int(height / (sizefactor / 2))
    w = int(width / (sizefactor / 2))
    img = edged.copy()
    # img = line_image.copy()
    img = cv2.resize(img, (w, h))  # Resize image
    cv2.imshow("DOG", img)
    cv2.waitKey(0)


    return edged

def align_screw(image, display, out_val) :
    height, width = image.shape
    display = display.copy()

    # image = image[150:int(height - 150), int(200):int(width - 400)]

    lines_img = image.copy()
    test = image.copy()



    #gray = cv2.cvtColor(lines_img, cv2.COLOR_BGR2GRAY)
    #gray = cv2.GaussianBlur(gray, (5, 5), 0)
    #sobelx = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=5)
    #sobely = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=5)

    # perform edge detection, then perform a dilation + erosion to
    # close gaps in between object edges

    #setup threshold arguments for line detector
    rho = 1
    theta = np.pi / 180
    threshold = 75  # 25  # maybe adjust			#todo asjust to work for smallest threshold
    min_line_length = 125                           #todo throw out outliers (horizontal lines)
    max_line_gap = 250  # higher  = more lines

    #get lines from the image
    edges = cv2.Canny(display, 35, 95)
    lines = cv2.HoughLinesP(edges, rho, theta, threshold, np.array([]), min_line_length, max_line_gap)


    #init variables for calculating angle to turn
    tot_angle = 0
    avg_slope = 0
    num_lines = 0
    line_good = 0

    cnt_neg = 0
    cnt_pos = 0

    #handle no lines drawn problem
    if lines is None:
        """
        height, width = lines_img.shape
        h = int(height / (sizefactor * 2))
        w = int(width / (sizefactor * 2))
        img = edges.copy()
        # img = line_image.copy()
        # img = cv2.resize(img, (w, h))  # Resize image
        cv2.imshow("Error1", img)
        cv2.waitKey(0)
        """
        return -1


    for line in lines:
        for x1, y1, x2, y2 in line:

            if (x2 - x1) != 0:                                      # if straight vertical line then angle is 90 cause probs w/ divide by 0 so simply keep track of these lines
                slope = (y2 - y1) / (x2 - x1)                       #get slope
                if abs(slope) < 1 :
                    continue
                if slope < 0:
                    cnt_neg = cnt_neg + 1                           #keep track of negative and positive values for tangent
                else:
                    cnt_pos = cnt_pos + 1
                slope = abs(slope)
                # print(slope)
                tot_angle = tot_angle + np.arctan(slope)            #keep total value of angles added together to get avg later
                num_lines = num_lines + 1
            else:
                line_good = line_good + 1                           #increment number of good vertical lines if exist
            cv2.line(display, (x1, y1), (x2, y2), (0, 0, 255), 10)  # draw line on image for debugging

    #flag for check if tangent was negative to turn in correct direction
    ang_neg = 0
    if cnt_neg > cnt_pos:
        ang_neg = 1

    avg_angle = abs(tot_angle / num_lines) #get avg angle

    angle = avg_angle * (180 / np.pi)    #convert to degrees
    line_good_tot = 90 * line_good      #calc total of good 90 degree lines in an angle
    angle = (angle * num_lines) + line_good_tot     #get the total angle of both added together
    angle = angle / (num_lines + line_good)         #divide by total number of lines to get avg

    height, width = lines_img.shape
    cnt_image = image.copy

    #setup turning angle based on the quadrant flag
    if ang_neg == 1:
        turn_ang = 90 - angle
    else:
        turn_ang = angle - 90


    #rotate image and fill empty space with nearest colored pixel
    cnt_img = ndimage.interpolation.rotate(test, turn_ang, mode="constant", cval = 0)  # angle - 90? /2?
    #cnt_img = test

    #change width of new image to remove as much of the filled image as possible (may not be neccesary)
    change_w = abs(height * np.sin(((angle - 90)) / 180 * np.pi))
    change_h = abs(width * np.sin(((angle - 90)) / 180 * np.pi))

    height, width = cnt_img.shape

    if abs(height - (2 * change_h)) < 500:
        change_h = change_h / 3
        change_w = change_w / 3

    if abs(width - (2 * change_w)) < 500:
        change_h = change_h / 3
        change_w = change_w / 3

    change_w = int(change_w)
    change_h = int(change_h)

    #cnt_img = cnt_img[int(change_h):int(height - change_h), int(change_w):int(width - change_w)]

    # image[int(change_h):int(height-change_h), int(change_w):int(width-change_w)] = cnt_img

    #output draw lines for debugging

    if out_val :
        height, width, _ = display.shape
        h = 800  # int(height / (sizefactor / 2.75))
        w = 300  # int(width / (sizefactor / 2.75))
        img = display.copy()
        # img = line_image.copy()
        img = cv2.resize(img, (w, h))  # Resize image
        winname = "Turn Lines"
        cv2.namedWindow(winname)  # Create a named window
        cv2.moveWindow(winname, 3745, 0)
        cv2.imshow(winname, img)
        cv2.waitKey(1500)

    return cnt_img.copy(), turn_ang

def background(test, out_val) :

    back = cv2.imread('./images/back' + str(1) + '.jpg')  #import stored background image

    #output images for viewing
    if out_val :
        cv2.destroyAllWindows()
        height, width, _ = back.shape
        h = 800  # int(height / (sizefactor / 2.75))
        w = 300  # int(width / (sizefactor / 2.75))
        img = cv2.resize(back, (w,h))
        winname = "Back"
        cv2.namedWindow(winname)  # Create a named window
        cv2.moveWindow(winname, 3185, 0)
        cv2.imshow(winname, img)
        cv2.waitKey(1500)
        #cv2.destroyAllWindows()
        height, width, _ = test.shape
        h = 800  # int(height / (sizefactor / 2.75))
        w = 300  # int(width / (sizefactor / 2.75))

        img = cv2.resize(test, (w, h))
        winname = "Screw"
        cv2.namedWindow(winname)  # Create a named window
        cv2.moveWindow(winname, 3185, 0)
        cv2.imshow(winname, img)
        cv2.waitKey(1500)
        #cv2.destroyAllWindows()


    #convert image to grayscale
    grayBack = cv2.cvtColor(back, cv2.COLOR_BGR2GRAY)
    grayForg = cv2.cvtColor(test, cv2.COLOR_BGR2GRAY)

    grayBack = cv2.GaussianBlur(grayBack, (17, 17), 0)  # (21,21)
    grayForg = cv2.GaussianBlur(grayForg, (17, 17), 0)  # (21,21)

    # compute the Structural Similarity Index (SSIM) between the two
    # images, ensuring that the difference image is returned
    (score, diff) = compare_ssim(grayBack, grayForg, full=True)
    diff = (diff * 255).astype("uint8")
    #print("SSIM: {}".format(score))

    # threshold the difference image, followed by finding contours to
    # obtain the regions of the two input images that differ
    thresh = cv2.threshold(diff, 0, 255,
                           cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

    thresh = cv2.dilate(thresh, None, iterations=1)  # 15
    thresh = cv2.erode(thresh, None, iterations=1)  #15

    if out_val :
        height, width = thresh.shape
        h = 800  # int(height / (sizefactor / 2.75))
        w = 300  # int(width / (sizefactor / 2.75))
        #print(h, w)
        img = cv2.resize(thresh, (w, h))
        winname = "Pre-turn"
        cv2.namedWindow(winname)  # Create a named window
        cv2.moveWindow(winname, 3470, 0)
        cv2.imshow(winname, img)
        cv2.waitKey(1500)
        #cv2.destroyAllWindows()





    thresh, turn_ang = align_screw(thresh, test, out_val)  #align screw with b&w image (may want to change)

    if out_val:
        height, width = thresh.shape
        h = 800  # int(height / (sizefactor / 2.75))
        w = 300  # int(width / (sizefactor / 2.75))
        img = cv2.resize(thresh, (w, h))
        winname = "Post-turn"
        cv2.namedWindow(winname)  # Create a named window
        cv2.moveWindow(winname, 4020, 0)
        cv2.imshow(winname, img)
        cv2.waitKey(1500)
        #cv2.destroyAllWindows()


    #find contours and return them with the image

    cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                            cv2.cv2.CHAIN_APPROX_NONE)# CHAIN_APPROX_TC89_L1

    return  cnts, thresh, turn_ang


def get_cnts(image, out_val):
    cnts, edged, turn_ang = background(image, out_val)  # get contour by using background detection

    # image = align_screw(image)

    #edged = origCntDetect(image)           #uses original detection algorithm

    # find contours in the edge map
    # done in detection now
    cnts, _ = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_NONE)  # CHAIN_APPROX_TC89_KCOS?



    #cnts = cnts[0] if imutils.is_cv2() else cnts[1]




    # sort the contours from left-to-right and initialize the
    (cnts, _) = contours.sort_contours(cnts)

    cntlist = []
    maxArea = 0
    notFirst = False

    for c in cnts:  # remove all contours objects except for the largest one (will be the screw)
        Area = cv2.contourArea(c)
        if Area > maxArea:  # check to see if contour is bigger than previous max
            if notFirst:
                cntlist.pop()  # pop off previous max
            notFirst = True
            cntlist.append(c)  # append new max (will be screw at the end)

            maxArea = Area  # set new max
    #print(maxArea)
    checkArea = 0
    if(maxArea < 17500 or maxArea > 200000) :
        checkArea = -1
    #draw blue contour around screw for debugging
    #cv2.drawContours(image, cntlist, -1, (255, 0, 0), 2)  # draw contour for debugging

    # show the output image

    								#connect dots
    """
    #image = ndimage.interpolation.rotate(image, turn_ang, mode="nearest")  # angle - 90? /2?
    orig = image.copy()									#blue dots
    height, width, channels = orig.shape
    h = int(height / (sizefactor /2))
    w = int(width / (sizefactor /2 ))
    #print(h,w)
    img = cv2.resize(orig, (w, h))  # Resize image				# could put a point at each postion of the drawn contour
    cv2.imshow("Image", img)
    cv2.waitKey(0)
    """
    # print(cntlist)

    image = ndimage.interpolation.rotate(image, turn_ang, mode="constant", cval=0)  # angle - 90? /2?



    return cntlist, image, checkArea, turn_ang


def screw_id(image_file, upsidedown, out_val):

    image = cv2.imread(image_file)  # args["image"])			#add args again
    if upsidedown:
        image = np.rot90(image)
        image = np.rot90(image)

    tflow = image.copy()

    #tflow = cv2.cvtColor(tflow, cv2.COLOR_BGR2GRAY)


    cntlist, image, checkArea, turn_ang = get_cnts(image, out_val)

    #tflow = ndimage.interpolation.rotate(image, turn_ang, mode="constant", cval=0)  # angle - 90? /2?


    screw = []  # 2 objects once quarter is removed
    screw.append(cntlist[0])


    # init variables
    screw = screw[0]

    screw = list(screw)


    top_x = 0
    ytop = 3000 #set to 3000 to get smallest number to take (will be less than 3000)
    ybottom = 0

    #set vals to 0
    xleft_avg = 0
    xright_tot = 0
    xright_avg = 0
    xleft_tot = 0
    ytop_tot = 0
    ytop_avg = 0
    ybot_tot = 0
    ybot_avg = 0

    #counts for averaging
    left_count = 1
    right_count = 1
    bot_count = 1
    top_count = 1

    #flags for switching from different parts of screw
    rightSide = False
    botSide = False
    topSide = False


    for i in range(0,(len(screw) - 1)) : #find top/bottom pixels of screw  #todo (solve sigle bad pixel problem)
        coord = screw[i]
        x, y = np.split(coord, [-1], axis=1)
        y = int(y)
        x = int(x)

        if y > ybottom :
            ybottom = y

        if y < ytop:
            ytop = y

    y_mid = ytop + int((ybottom - ytop) / 2)        #find mid location of screw

    top = []
    bot = []

    for i in range(0, len(screw)):
        coord = screw[i]  # find distance between contours with similar (3 pixels) y values
        # coord = coord[0]
        x, y = np.split(coord, [-1], axis=1)
        y = int(y)
        x = int(x)

        if y <= y_mid:
            top.append([x, y])
        else:
            bot.append([x, y])

    t = []  # turn each object to contour numpy array
    t.append(top)
    top = np.array(t).astype('int32')
    b1 = []
    b1.append(bot)
    bot = np.array(b1).astype('int32')

    top_area = cv2.contourArea(top)
    bot_area = cv2.contourArea(bot)

    #print(top_area)
    #print(bot_area)

    img_cp = image.copy()

    cv2.drawContours(img_cp, top, -1, (0, 255, 0), 2)  # uncomment to see the contours drawn
    cv2.drawContours(img_cp, bot, -1, (0, 0, 255), 2)  #
    cv2.drawContours(img_cp, screw, -1, (255, 0, 0), 2)  # draw contour for debugging

    if out_val:
        orig = img_cp.copy()
        height, width, channels = orig.shape
        h = 800 #int(height / (sizefactor / 2.75))
        w = 300 #int(width / (sizefactor / 2.75))
        img = cv2.resize(orig, (w, h))  # Resize image
        winname = "Screw"
        #print(h,w)
        cv2.namedWindow(winname)  # Create a named window
        cv2.moveWindow(winname, 4310, 0)
        cv2.imshow(winname, img)
        cv2.waitKey(2000)
        cv2.destroyAllWindows()

    # show the output image
    # connect dots

    if top_area < bot_area:				#uncomment upside down
        cv2.destroyAllWindows()
        print("UpsideDown")
        return -1

    for i in range(int(len(screw)/5), ((len(screw) - 1) - int(len(screw)/5)) ):  # find average x pixel on left and right side of shaft
        coord = screw[i]                                                         #assuming most of screw tail is in the middle 3/5 of screw array
        x, y = np.split(coord, [-1], axis=1)
        y = int(y)
        x = int(x)

        if not(rightSide) :                                         #start on the left
            if ybottom-y > 10 :                                     #ensure pixel is near the very bottom of the screw
                if (left_count) < 5 or (abs(x - xleft_avg) < 35):   #make sure pixel is close to other pixels seen before
                    xleft_tot = xleft_tot + x                       #add pixel to left average
                    xleft_avg = int(xleft_tot / left_count)
                    left_count = left_count + 1
            else :
                rightSide = True
        else :
            if ybottom - y > 20 :                                       #higher threshold to ensure pixels on left aren't part of the average
                if (right_count < 5) or (abs(x - xright_avg) < 35):
                    xright_tot = xright_tot + x
                    xright_avg = int(xright_tot / (right_count))
                    right_count = right_count + 1





    for i in range(0, (len(screw) - 1)):  # find average top and bottom of screw
        coord = screw[i]
        x, y = np.split(coord, [-1], axis=1)
        y = int(y)
        x = int(x)

        if not(botSide) or topSide :                                        #need 2 flags because left side of top starts and right side of top is at the end of screw[]
            if x - xleft_avg > 10  and xright_avg - x > 10:                 #only take pixels between avg width - 20
                if top_count < 5 or abs(y - ytop_avg) < 50 :               #ensure pixel is close to pixels seen before
                    ytop_tot = ytop_tot + y                                #add pixel to average
                    ytop_avg = int(ytop_tot / top_count)
                    top_count = top_count + 1
            else :
                botSide = True                                              #switch to bottom
        else :
            #print("x: ",x," y: ",y)
            #print(xleft_avg, "  : ", xright_avg)
            if x - xleft_avg > 15 and xright_avg - x > 15:          #increase threshold to ensure top pixels not added to bottom
                #print("here")
                if bot_count < 5 or abs(y - ybot_avg) < 50:
                    ybot_tot = ybot_tot + y
                    ybot_avg = int(ybot_tot / bot_count)
                    bot_count = bot_count + 1
            else :
                if xright_avg - x < 5 :                                #go back to top once bottom is done
                    topSide = True

    if(ybot_avg < 25 or abs(ybot_avg - ytop_avg) < 150 or abs(ybot_avg-ybottom) > 100 ):
        ybot_avg = ybottom
    #if( abs(ytop_avg-ytop) > 75) :
       # ytop_avg = ytop

    #set output size variables
    ylength = ybot_avg - ytop_avg
    xwidth = xright_avg - xleft_avg

    height, width, channels = image.shape

    y_top_crop = ytop_avg - 20
    y_bot_crop = ybot_avg + 20
    x_right_crop = xright_avg + 100
    x_left_crop = xleft_avg - 100

    if(y_top_crop < 0) : y_top_crop = 0
    if(x_left_crop < 0) : x_left_crop = 0
    if(y_bot_crop > height) : y_bot_crop = height
    if(x_right_crop > width) : x_right_crop = width


    image_cnt = image.copy()

    #cv2.drawContours(image, screw, -1, (255, 0, 0), 2)  # draw contour for debugging
    #output image with debug lines
    """
    orig = image_cnt.copy()
    height, width, channels = orig.shape
    h = int(height / (sizefactor / 2))
    w = int(width / (sizefactor / 2))
    img = cv2.resize(orig, (w, h))  # Resize image
    cv2.imshow("Image", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    """
    # draw lines on image to debug functionality
    cv2.line(image, (xleft_avg, ytop_avg), (xleft_avg, ybot_avg), (255, 255, 0), 5)  # baby blue left
    cv2.line(image, (xright_avg, ytop_avg), (xright_avg, ybot_avg), (0, 255, 0), 5)  # green right
    cv2.line(image, (xleft_avg, ytop_avg), (xright_avg, ytop_avg), (0, 255, 255), 5)  # yellow top
    cv2.line(image, (xleft_avg, ybot_avg), (xright_avg, ybot_avg), (0, 0, 255), 5)  # red bot
    #cv2.line(image, (xleft_avg, y_mid), (xright_avg, y_mid), (0, 0, 255), 5)  # red bot

   # image = image[y_top_crop: y_bot_crop, x_left_crop:x_right_crop]
    #tflow = tflow[y_top_crop: y_bot_crop, x_left_crop:x_right_crop]

    if out_val:
        orig = image.copy()
        height, width, channels = orig.shape
        h = 800  # int(height / (sizefactor / 2.75))
        w = 300  # int(width / (sizefactor / 2.75))
        img = cv2.resize(orig, (w, h))  # Resize image
        winname = "L/W Box"
        cv2.namedWindow(winname)  # Create a named window
        cv2.moveWindow(winname, 3400, 0)
        cv2.imshow(winname, img)
        cv2.waitKey(1000)
        #cv2.destroyAllWindows()

        orig = tflow.copy()
        height, width, channels = orig.shape
        h = 800  # int(height / (sizefactor / 2.75))
        w = 300  # int(width / (sizefactor / 2.75))
        img = cv2.resize(orig, (w, h))  # Resize image
        winname = "Tflow Input"
        cv2.namedWindow(winname)  # Create a named window
        cv2.moveWindow(winname, 3800, 0)
        cv2.imshow(winname, img)
        cv2.waitKey(1500)



    if (ylength < 200 or xwidth < 50 or checkArea==-1 or ybot_avg==0):
        return -2

    scipy.misc.imsave('./images/tf.jpg', tflow)

    return tflow, ylength, xwidth  # replace -1 with image for head type detection




def detect(image, out_val):
    cropped = screw_id(image, False, out_val)  # false means right side up
    if cropped == -1:                   #returns -1 if image is upside down
        cropped = screw_id(image, True, out_val)

    elif cropped == -2:
        return -2,-2,-2

    return cropped


def find_head_type(screw, out_val):  # todo get this tensorflow stuff to run without errors cur err is with image shape
    #return -1
    start = time.perf_counter()
    # Name of the directory containing the object detection module we're using
    MODEL_NAME = 'screw_model'
    IMAGE_NAME = './images/ace1.jpg'

    # Grab path to current working directory
    CWD_PATH = os.getcwd()

    # Path to frozen detection graph .pb file, which contains the model that is used
    # for object detection.
    PATH_TO_CKPT = os.path.join(CWD_PATH, MODEL_NAME, 'frozen_inference_graph.pb')

    # Path to label map file
    PATH_TO_LABELS = os.path.join(CWD_PATH, 'training', 'screw_labelmap.pbtxt')

    # Path to image
    PATH_TO_IMAGE = os.path.join(CWD_PATH, IMAGE_NAME)

    # Number of classes the object detector can identify
    NUM_CLASSES = 4

    # Load the label map.
    # Label maps map indices to category names, so that when our convolution
    # network predicts `5`, we know that this corresponds to `king`.
    # Here we use internal utility functions, but anything that returns a
    # dictionary mapping integers to appropriate string labels would be fine
    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                                use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    # Load the Tensorflow model into memory.
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        sess = tf.Session(graph=detection_graph)

    # Define input and output tensors (i.e. data) for the object detection classifier

    # Input tensor is the image
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

    # Output tensors are the detection boxes, scores, and classes
    # Each box represents a part of the image where a particular object was detected
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represents level of confidence for each of the objects.
    # The score is shown on the result image, together with the class label.
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

    # Number of objects detected
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # Load image using OpenCV and
    # expand image dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value
    #image = cv2.imread(IMAGE_NAME)

    image = screw
    #image =  Image.open('./images/tf.jpg')
    #image.thumbnail((38, 120), )
    #image.save('./images/tf.jpg', dpi=(10, 10))
    #image = Image.open('./images/tf.jpg')
    """
    orig = image.copy()
    height, width, channels = orig.shape
    h = int(height / (sizefactor / 2))
    w = int(width / (sizefactor / 2))
    img = cv2.resize(orig, (w, h))  # Resize image
    cv2.imshow('Object detector', img)

    # Press any key to close the image
    cv2.waitKey(0)
    """
    #print("HELLO")


    image_expanded = np.expand_dims(image, axis=0)
    end = time.perf_counter()
    print(end - start)
    start = time.perf_counter()
    # Perform the actual detection by running the model with the image as input
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})

    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})

    # Draw the results of the detection (aka 'visulaize the results')

    end = time.perf_counter()

    """
    Vis_utils.visualize_boxes_and_labels_on_image_array(
        image,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=8,
        min_score_thresh=0.80)
    """
    # All the results have been drawn on image. Now display the image.
    print(end - start)
    id = np.squeeze(scores)[0]
    percentage = np.squeeze(classes)[0]
    print(id, percentage)

    if out_val :
        orig = image.copy()
        #height, width, channels = orig.shape
        #h = 800  # int(height / (sizefactor / 2.75))
        #w = 300  # int(width / (sizefactor / 2.75))
        #img = cv2.resize(orig, (w, h))  # Resize image
        winname = "TensorFlow"
        cv2.namedWindow(winname)  # Create a named window
        cv2.moveWindow(winname, 4200, 0)
        cv2.imshow(winname, orig)

        # Press any key to close the image
        cv2.waitKey(2000)

        # Clean up
        cv2.destroyAllWindows()

    return id, percentage


def check_match(ht, percent, l, w):              #todo adjust thresholds for final decision placement

    if ht == -1 :
        for i in range(0, len(screw_list)):                             #todo add leway for third condition when 2 are correct
            if abs(l - screw_list[i][1]) < 40 and abs(w - screw_list[i][2]) < 22:  # add screw to previously seen screw bin {diam/ht good give length some room
                avg_l = int((l + screw_list[i][1] * screw_list[i][3]) / (screw_list[i][3] + 1))               # adjust l/w to be an average if calculared values for screws in the bin
                avg_w = int((w + screw_list[i][2] * screw_list[i][3]) / (screw_list[i][3] + 1))
                new_num = screw_list[i][3] + 1                                                                #new_num = number of screws in the bin
                del screw_list[i]
                screw_list.insert(i, [ht, avg_l, avg_w, new_num])
                return i + 1


                    #pos positions 1,2,3,4,5,6
        if len(screw_list) < 6:                         #add screw not seen before
            screw_list.append([ht, l, w, 1])
            return (len(screw_list))
        else:
            return 6                                    #send to 6th bin

    else :
        for i in range(0, len(screw_list)):  # todo add leway for third condition when 2 are correct
            if ht == screw_list[i][0] and abs(l - screw_list[i][1]) < 40 and abs(w - screw_list[i][2]) < 20:  # add screw to previously seen screw bin {diam/ht good give length some room
                avg_l = int((l + screw_list[i][1] * screw_list[i][3]) / (screw_list[i][
                                                                             3] + 1))  # adjust l/w to be an average if calculared values for screws in the bin
                avg_w = int((w + screw_list[i][2] * screw_list[i][3]) / (screw_list[i][3] + 1))
                new_num = screw_list[i][3] + 1  # new_num = number of screws in the bin
                del screw_list[i]
                screw_list.insert(i, [ht, avg_l, avg_w, new_num])
                return i + 1

                # pos positions 1,2,3,4,5,6
        if len(screw_list) < 6:  # add screw not seen before
            screw_list.append([ht, l, w, 1])
            return (len(screw_list))
        else:
            return 6
