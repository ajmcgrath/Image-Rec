# Servo Control
import time
import wiringpi
import random
import argparse
import sys

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--spot", required=True,
	help="path to the input image")
args = vars(ap.parse_args())

turn = args["spot"]
turn = int(turn)

# use 'GPIO naming'
wiringpi.wiringPiSetupGpio()
 
# set #18 to be a PWM output
wiringpi.pinMode(18, wiringpi.GPIO.PWM_OUTPUT)
 
# set the PWM mode to milliseconds stype
wiringpi.pwmSetMode(wiringpi.GPIO.PWM_MODE_MS)
 
# divide down clock
wiringpi.pwmSetClock(192)
wiringpi.pwmSetRange(2000)
 
delay_period = 1.5

def servo(turn):
    if turn == 1:
        wiringpi.pwmWrite(18, 50)
        time.sleep(delay_period)
        print ("0 degrees \n")
    elif turn == 2:
        wiringpi.pwmWrite(18, 90)
        time.sleep(delay_period)
        print ("36 degrees \n")
    elif turn == 3:
        wiringpi.pwmWrite(18, 130)
        time.sleep(delay_period)
        print ("72 degrees \n")
    elif turn == 4:
        wiringpi.pwmWrite(18, 170)
        time.sleep(delay_period)
        print ("108 degrees \n")
    elif turn == 5:
        wiringpi.pwmWrite(18, 210)
        time.sleep(delay_period)
        print ("144 degrees \n")
    elif turn == 6:
        wiringpi.pwmWrite(18, 250)
        time.sleep(delay_period)
        print ("180 degrees \n")
        
servo(turn)
