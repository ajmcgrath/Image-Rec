#reverse is currently forward

from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np
import PIL
import argparse
import sys
import scipy.misc
from PIL import Image


# Servo Control
import time
import wiringpi
import random
import argparse
import sys

# use 'GPIO naming'
wiringpi.wiringPiSetupGpio()
 
# set #18 to be a PWM output
wiringpi.pinMode(17, 1)
wiringpi.pinMode(22, 1)


 


def forward():
    wiringpi.digitalWrite(17,0)
    wiringpi.digitalWrite(22,1)
    """
    wiringpi.digitalWrite(23,0)
    wiringpi.digitalWrite(24,1) """
    
def reverse():
    wiringpi.digitalWrite(17,1)
    wiringpi.digitalWrite(22,0)
    """
    wiringpi.digitalWrite(23,1)
    wiringpi.digitalWrite(24,0) """
    
def stop():
    wiringpi.digitalWrite(17,0)
    wiringpi.digitalWrite(22,0)
        


ap = argparse.ArgumentParser()
ap.add_argument("-z", "--spot", required=True,
	help="path to the input image")
args = vars(ap.parse_args())

location = args["spot"]


# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
#camera.color_effects = (128,128)
camera.resolution = (640,480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))
rawCapture2 = PiRGBArray(camera, size=(2048, 1536))

width = 400
height = 400


#print("first check")

def screwDetect2():
   
    #print("check if in function")
    #reference image that will be used to detect screw
    
    #allow the camera to warmup
    time.sleep(0.1)
    # clear the stream in preparation for the next frame
    #key boolean for breaking out of this function
    screwDetected = False
    backToWhite = False
    takePic = False
    
    
    camera.resolution = (640,480)
    camera.capture(rawCapture, format="bgr", use_video_port=True)
    
    orig = rawCapture.array
    
    rawCapture.truncate(0)
    
    orig = Image.open('orig.jpg')
    
  
    
    orig = np.asarray(orig).copy()
    #orig = orig[0:480, 255:365]
    
    #cv2.imshow("image", orig)
    #cv2.waitKey(0)
    
    #print("start conveyor")
    reverse()
    #time.sleep(5)
   
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	# grab the raw NumPy array representing the image - this array will be 3D, representing the width, height, and # of channels
        image = frame.array
       
        rawCapture.truncate(0)
        
        image = np.asarray(image).copy()
        image = image[0:480, 255:365]     #<---- changed
        
        #image = image[0:25, 290:365]
       
        #cv2.imshow("image", image)
        #key = cv2.waitKey(1) & 0xFF
        #this is the first check. If in a previous frame we detected a screw coming into through the top of the picture we check if all of it is in frame.
        loc = 0
        cor = 0
        incor = 0
        if(backToWhite == False):
            
            for x in range(0, 110, 1):
                for y in range(0,25,1) : #for y in range(479,455,-1) :
                    a = int(image[y,x][1])
##                    b = int(orig[y+455,x][1])
                    dif = abs(a-b)
                                               
                    if(dif > 55) :
                            
                        incor = incor + 1
                        loc = x
                           
                    else :
                         cor = cor + 1
                
            #print("incor 3 ",incor)
            if incor  < 100 :
                #cv2.imshow("image", image)
                #cv2.waitKey(0)
                #cv2.imshow("image", orig)
                #cv2.waitKey(0)
                   
                #print("LookForScrew")
                backToWhite = True
                orig = np.asarray(image).copy() 
                 
        else :
            #return

            if(screwDetected == False):
                for x in range(0, 110, 1):
                    for y in range(0,25,1) : #for y in range(479,455,-1) :
                        a = int(image[y,x][1])
                        b = int(ref[y,x][1])
                        dif = abs(a-b)
                    
                        
                        if(dif > 25) :
                           
                            incor = incor + 1
                            loc = x
                            
                        else :
                            cor = cor + 1
                #print("incor ",incor)
                if incor  > 350: #250 
                    screwDetected = True
                    
                
            else :
                for x in range(0, 110, 1):
                    for y in range(0,25,1) : #for y in range(479,455,-1) :
                        a = int(image[y,x][1])
                        b = int(orig[y,x][1])
                        dif = abs(a-b)
                    
                        
                        if(dif > 30) :
                            
                            incor = incor + 1
                            loc = x
                           
                        else :
                            cor = cor + 1
                #print("incor 2 ",incor)
                if incor  < 250:
                   
                    stop()
                    #print("Full Screw Detected")
                    
                    return
        ref = np.asarray(image).copy()
    

def screwDetection():
    reverse()
    time.sleep(0.2)
    start = 0
    end = 0
    #print("check if in function")
    #reference image that will be used to detect screw
    
    #allow the camera to warmup
    #time.sleep(0.1)
    # clear the stream in preparation for the next frame
    #key boolean for breaking out of this function
    screwDetected = False
    takePic = False
    endThresh = 0
    firstPic = True
    
    #camera.resolution = (2048, 1536)
    #camera.capture(rawCapture2, format="bgr", use_video_port=False)
    #scipy.misc.imsave("back.jpg", rawCapture2.array[0:1536, 500 : 1500])
    #rawCapture2.truncate(0)
    
    #camera.resolution = (640,480)
  
    camera.resolution = (640,480)
    camera.capture(rawCapture, format="bgr", use_video_port=True)
    
    orig = rawCapture.array
    
    rawCapture.truncate(0)
    
    orig = Image.open('orig.jpg')
    
  
    
    orig = np.asarray(orig).copy()
  
    #ref = ref.point(lambda x: 0 if x<128 else 255, '1')
    #cv2.imshow("image", ref)
    #cv2.waitKey(0)
    
    # allow the camera to warmupfirstPic = True
    # capture frames from the camera print("check if in function")
    #reference image that will be used to detect screw
    #camera.capture(rawCapture, format="bgr", use_video_port=True)
    #allow the camera to warmup
    #time.sleep(0.1)
    # = rawCapture.array
    # clear the stream in preparation for the next frame
    #rawCapture.truncate(0)
    #key boolean for breaking out of this function
    
   # print("photo taken")
    
    #ref = np.asarray(ref).copy()
    #ref[ref<200] = 0
    #ref[ref>=200] = 255
    #print(ref)
    #ref = ref[0:25, 155:330]
    #ref = ref.point(lambda x: 0 if x<128 else 255, '1')
    #cv2.imshow("image", ref)
    #cv2.waitKey(0)
    
    # allow the camera to warmup
    
    count = 0
   
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	# grab the raw NumPy array representing the image - this array will be 3D, representing the width, height, and # of channels
        if firstPic :                       #store reference for first picture
            ref = frame.array
            rawCapture.truncate(0)
            ref = np.asarray(ref).copy()
            
            ref = ref[0:480, 190:320]     
            #orig = ref.copy()
            #cv2.imshow("image", ref)
            #cv2.waitKey(0)
            firstPic = False
        else :
            #cv2.imshow("image", frame.array)
            #cv2.waitKey(0)
                
            image = frame.array
            #cv2.imshow("Frame", image)
            #key = cv2.waitKey(1) & 0xFF
            rawCapture.truncate(0)
            
            image = np.asarray(image).copy()
            image = image[0:480, 190:320]     #<---- changed   #image = image[0:480, 290:365]     #<---- changed
            #image[image<128] = 0
            #image[image>=128] = 255
            #image[image<200] = 255
            #image[image>=200] = 0
            #cv2.imshow("image", image)
            #key = cv2.waitKey(1) & 0xFF
            #this is the first check. If in a previous frame we detected a screw coming into through the top of the picture we check if all of it is in frame.
            loc = 0
            cor = 0
            incor = 0
            if(screwDetected == False):             #front of screw not yet seen
                for x in range(0, 130, 1):
                    for y in range(0,25,1) : #for y in range(479,455,-1) :
                        a = int(image[y,x][1])
                        b = int(ref[y,x][1])
                        dif = abs(a-b)                      #compare pixel of current frame and previous frame
                        
                        #,type(dif)\\ , difa, difb, difc)
                        if incor > 500 :                    #if greater than target threshold break out (increase speed)
                            break
                        elif(dif > 25) :                    #add pixel to incorect count if different (tested theshold based on lighting)
                            #print(dif)
                            #screwDetected = True
                            incor = incor + 1
                            loc = x
                            #break
                        else :
                            cor = cor + 1
                #print("incor ",incor,":",dif)
                if incor  > 500: #250                       #enter here when screw entry detected
                    screwDetected = True
                    start = time.perf_counter()
                    end = time.perf_counter()
                    #orig = ref.copy()
                    #orig = ref.copy()
                    #endThresh = incor
                    
                    """
                    for x in range(0, 305, 1):
                        for y in range(479,455,-1) :
                            a = int(orig[y,x][1])
                            b = int(ref[y,x][1])
                            dif = abs(a-b)
                        
                            #,type(dif)\\ , difa, difb, difc)
                            if(dif > 30) :
                                #print(dif)
                                #screwDetected = True
                                incor = incor + 1
                                loc = x
                                #print("screw detected")
                                #break
                            else :
                                cor = cor + 1
                    
                    #print("End: ", endThresh)
                    """
                ref = np.asarray(image).copy()
            #if(count % 10) == 0:
                
            else :
                if(end-start) < 3 :                 #allow for three seconds of time before throwing bad detect error
                    #print(start-end)
                    for x in range(0, 130, 1):
                        for y in range(0,25,1) : #for y in range(479,455,-1) :
                            a = int(image[y,x][1])
                            b = int(orig[y,x][1])
                            dif = abs(a-b)          #compare pixel of current frame and orignal (conveyor blank) frame
                        
                            #,type(dif)\\ , difa, difb, difc)
                            #if incor > 550 :
                                #break
                            if(dif > 62) :   #add pixel to incorect count if different (tested theshold based on lighting)
                                #print(dif)
                                #screwDetected = True
                                incor = incor + 1
                                loc = x
                                #print("screw detected")
                                #break
                            #else :
                                #cor = cor + 1
                    #print("incor 2 ",incor,":",dif)
                    #camera.resolution = (1600,1200)
                    #vals = (10,5,1)
                    #camera.capture(location,format="jpeg",use_video_port=False)
                    #camera.resolution = (640,480)
                    if incor  < 650:
                        #forward()  
                        #time.sleep(0)
    
                        forward()
                        time.sleep(0.2)
                        stop()
                        #time.sleep(5)
                        #print("Full Screw Detected")
                        #scipy.misc.imsave(location, frame.array[0:960, 345:850])
                        #scipy.misc.imsave(location, image)
                        #camera.resolution = (1600,1200)
                        #vals = (10,5,1)
                        #camera.capture(location,format="jpeg",use_video_port=False)
                        return True
                    end = time.perf_counter()
                else :
                    return False
                    

    
def takePic() :
        #time.sleep(0.2)
        camera.resolution = (1600,1200)
        #vals = (10,5,1)
        camera.capture(location,format="jpeg",use_video_port=False)
        #cv2.imshow("image", pic)
        pic = Image.open(location)
        
        
        
        
       
        pic = pic.crop((470,0,845,1200)).convert('L')
        x = pic.save(location)
        #image = np.asarray(pic).copy()
        
        #cv2.imshow("image", image)
        #cv2.waitKey(0)
        
        #pic.show()
        #print("taken")
        
        
#main        
val = screwDetection()
#screwDetect2()
if not(val) :
    print("Bad Detect")
    
    
else:
    takePic()
    print("Done")
    #time.sleep(5)
    reverse()


