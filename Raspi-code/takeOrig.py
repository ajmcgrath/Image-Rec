from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np
import PIL
import argparse
import sys
import scipy.misc
from PIL import Image

# Servo Control
import time
import wiringpi
import random
import argparse
import sys


def takePic() :
        camera.resolution = (1600,1200)
        #vals = (10,5,1)
        camera.capture('d.jpg',format="jpeg")
        #cv2.imshow("image", pic)
        pic = Image.open('d.jpg')
       
        pic = pic.crop((470,0,845,1200)).save('d.jpg')
        #pic.show()
        print("taken")

camera = PiCamera()

camera.resolution = (640,480)
rawCapture = PiRGBArray(camera, size=(640, 480))
camera.capture(rawCapture, format="bgr", use_video_port=True)


orig = rawCapture.array

rawCapture.truncate(0)

#orig = cv2.imread('f.jpg')

#orig.crop(0,255,480,365)

orig = np.asarray(orig).copy()
orig = orig[0:480, 190:320]
#orig.crop(0,255,480,365).save('orig.jpg')
#orig = PIL.Image.fromarray(orig)
#orig.save('orig.jpg')
#cv2.imshow("image", orig)
#cv2.waitKey(0)

#scipy.misc.imsave('orig.jpg',orig)

takePic()

print("Done")
